#!/bin/sh
#
# Install Packages for iPreso player
#

export LANG=fr_FR.UTF-8
export DEBIAN_FRONTEND=noninteractive

if [ -f "/tmp/env" ];
then
    . /tmp/env
else
    echo "ATTENTION: Aucune variable d'environnement chargée."
fi

# Do not get the LD_LIBRARY_PATH from the parent environment
# (outside of the chroot)
export LD_LIBRARY_PATH=""

# Used by ATD daemon to start
chmod a+rw /dev/null

failure () {
    echo "Echec"
    echo "$1"
    read choice
    exit 1
}


# Upgrade original root archive to the last packages of the current
# distribution with security updates
echo -n "- Mise à jour du système initial... "
RES=$( apt-get update 2>&1 ) || failure "${RES}"
RES=$( apt-get dist-upgrade -y --force-yes 2>&1 ) || failure "${RES}"
RES=$( apt-get autoremove -y --force-yes 2>&1 ) || failure "${RES}"
echo "Ok"

echo -n "- Installation du Bootscreen... "
RES=$( apt-get install -y --allow-unauthenticated plymouth 2>&1 ) || failure "${RES}"
echo "Ok"

echo -n "- Détection de la carte réseau... "
REALTEK=$( /usr/bin/lspci -n | grep -E "10ec:816" 2>&1 )
if [ -n "${REALTEK}" ] ; then
    echo "Realtek."
    echo -n "   * Installation des Firmware Realtek... "
    RES=$( apt-get install -y --allow-unauthenticated firmware-realtek 2>&1 ) || failure "${RES}"
    echo "Ok"
else
    echo "Standard"
fi

echo -n "- Détection du disque... "
if [ "${HD_TYPE}" == "nvme" ] ; then
    echo "NVM Express"
    echo -n "   * Installation des outils NVMe... "
    RES=$( apt-get install -y --allow-unauthenticated nvme-cli 2>&1 ) || failure "${RES}"
    echo "Ok"
else
    echo "Standard"
fi

#echo -n "- Détection de la carte vidéo... "
#NVIDIA=`/usr/bin/lspci -n | grep -E "10de:(0874|0a64)"`
#if [ "$NVIDIA" != "" ];
#then
#    echo "NVidia"
#    echo -n "- Installation des drivers NVidia... "
#    RES=$( apt-get install -y --allow-unauthenticated \
#                nvidia-kernel-common \
#                nvidia-kernel-`uname -r` \
#                libgl1-mesa-glx \
#                nvidia-glx 2>&1 )
#    if [ "$?" != "0" ] ; then
#        echo "Echec"
#        echo "$RES"
#        read choice
#        exit 1
#    fi
#    echo "Ok"
#else
#    echo "Intel"
#    echo -n "- Installation des drivers Intel... "
#    RES=$( apt-get install -y --allow-unauthenticated \
#                xserver-xorg-video-intel \
#                libdrm-intel1 2>&1 )
#    if [ "$?" != "0" ] ; then
#        echo "Echec"
#        echo "$RES"
#        read choice
#        exit 1
#    fi
#    echo "Ok"
#fi

# Installation of third-party software, used by plugins
#echo -n "- Installation des logiciels nécessaires aux plugins principaux... "
#RES=$( apt-get install -y --allow-unauthenticated \
#                       -o dpkg::options::="--force-overwrite" \
#                       dpkg \
#                       ffmpeg               \
#                       freeglut3            \
#                       gnash                \
#                       imagemagick          \
#                       lsof                 \
#                       midori               \
#                       swftools             \
#                       ttf-dejavu-extra     \
#                       vlc                  \
#                       wmctrl               \
#                       x11-xserver-utils 2>&1 )
#echo "Ok"

echo -n "- Installation des drivers non-free et dépendances pour la partie graphique... "
RES=$( apt-get install -y --allow-unauthenticated firmware-linux-nonfree 2>&1 ) || failure "${RES}"
RES=$( apt-get install -y --allow-unauthenticated libdrm-intel1 2>&1 ) || failure "${RES}"
RES=$( apt-get install -y --allow-unauthenticated firmware-iwlwifi 2>&1 ) || failure "${RES}"
RES=$( apt-get install -y --allow-unauthenticated compton 2>&1 ) || failure "${RES}"
echo "Ok"

echo -n "- Installation des capteurs hardware... "
RES=$( apt-get install -y --allow-unauthenticated lm-sensors 2>&1 ) || failure "${RES}"
echo "Ok"

# Installation des scripts et configuration hardware et de leur spécialisation sur matériel Axiomtek

echo -n "- Installation des scripts et configuration hardware... "
RES=$( apt-get install -y --allow-unauthenticated ihardwareconfiguration 2>&1 ) || failure "${RES}"
echo "Ok"

echo -n "- Installation du porte-clefs Napafilia... "
RES=$( apt-get install -y --allow-unauthenticated napafilia-keyring 2>&1 ) || failure "${RES}"
echo "Ok"

echo -n "- Installation des outils nécessaires à l'installation... "
# - lsb-release: pour déterminer la distribution à configurer dans /etc/apt/sources.list.d/*
RES=$( apt-get install -y --allow-unauthenticated lsb-release 2>&1 ) || failure "${RES}"
echo "Ok"

# iWatchdog installation
echo -n "- Installation de iWatchdog... "
RES=$( apt-get install -y --allow-unauthenticated \
                        -o dpkg::options::="--force-overwrite" \
                        iwatchdog 2>&1 ) || failure "${RES}"
echo "Ok"

# iBox installation
echo -n "- Installation de iPlayer et de ses dépendances... "
RES=$( apt-get install -y --allow-unauthenticated \
                        -o dpkg::options::="--force-overwrite" \
                        iplayer 2>&1 ) || failure "${RES}"
echo "Ok"

# iBox installation
echo -n "- Installation des plugins de base... "
RES=$( apt-get install -y --allow-unauthenticated \
                        -o dpkg::options::="--force-overwrite" \
                        iplayer-powerctrl \
                        iplayer-videosmpv \
                        iplayer-images 2>&1 ) || failure "${RES}"
echo "Ok"

# Remove mails system if exist
# Goal is to not fill the HDD with useless email
if [ -f "/etc/init.d/exim4" ] ; then
    echo -n "- Suppression du MTA... "
    RES=$( /etc/init.d/exim4 stop 2>&1 )
    RES=$( killall exim4 )
    RES=$( apt-get purge exim4-daemon-light exim4-config exim4-base 2>&1 ) || failure "${RES}"
    echo "Ok"
fi

echo -n "- Stopping automatically loaded services... "
# Each running services shall be stopped in order to 
# allow the parent process to umount the whole disk
if [ -f "/etc/init.d/syslog-ng" ] ; then
    RES=$( /etc/init.d/syslog-ng stop 2>&1 )
fi
if [ -f "/etc/init.d/rsyslog" ] ; then
    RES=$( /etc/init.d/rsyslog stop 2>&1 )
fi
if [ -f "/etc/init.d/stunnel4" ] ; then
    RES=$( /etc/init.d/stunnel4 stop &> /dev/null )
fi
if [ -f "/etc/init.d/iplayer" ] ; then
    RES=$( /etc/init.d/iplayer stop 2>&1 )
fi
if [ -f "/etc/init.d/rng-tools" ] ; then
    RES=$( /etc/init.d/rng-tools stop 2>&1 )
fi
if [ -f "/etc/init.d/dbus" ] ; then
    RES=$( /etc/init.d/dbus stop 2>&1 )
fi
if [ -f "/etc/init.d/atd" ] ; then
    RES=$( /etc/init.d/atd stop 2>&1 )
fi
if [ -f "/etc/init.d/udev" ] ; then
    RES=$( /etc/init.d/udev stop 2>&1 )
fi
RES=$( kill -9 `pidof sshd` 2>&1 )
echo "Ok"

echo -n "- Umounting auto-mounted directories... "
if [ -d "/var/cache/iplayer" ] ; then
    mount | grep -q /var/cache/iplayer
    if [ "$?" == "0" ] ; then
        RES=$( umount /var/cache/iplayer 2>&1 ) || failure "${RES}"
    fi
fi
echo "Ok"
